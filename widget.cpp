#include "widget.h"
#include "ui_widget.h"
#include "mymedia.h"


Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);

    initUi();
}

Widget::~Widget()
{
    delete ui;
}


void Widget::initUi() {


    mainLayout = new QVBoxLayout(this);

    //创建封装Media
    MyMedia *media = new MyMedia;

    mainLayout->addWidget(media);
    media->show();
}

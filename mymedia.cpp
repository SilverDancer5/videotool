#include "mymedia.h"
#include <QPushButton>
#include <QVideoWidget>
#include <QFileInfo>
#include <QListWidgetItem>
#include <QFileDialog>
#include <QTime>
#include <QAction>
#include <QMenu>
#include <QContextMenuEvent>



MyMedia::MyMedia(QWidget *parent) : QWidget(parent)
{
    initUi();
}


/*
 * @Brief:
 * @Param:  音量
 * @Return: NULL
 */
void MyMedia::on_voluSlider(int val) {
    player->setVolume(val);
    qDebug() << "音量:" << val;
}

void MyMedia::initVolume(int val) {
    player->setVolume(val);
}

void MyMedia::initSlider(int val) {
    voluSlider->setValue(val);
}

/*
 * @Brief:
 * @Param:  进度
 * @Return: NULL
 */
void MyMedia::on_progressSlider(int val) {
    player->setPosition(player->duration()*val/100);
    qDebug() << "进度:" << val;
}

/*
 * @Brief:
 * @Param:  槽
 * @Return: NULL
 */
void MyMedia::on_ply_clicked() {
    showVideo(isPlay);
    qDebug() << "播放列表视频数量:" << playList->currentIndex();
}

void MyMedia::on_open_clicked() {
    QStringList fileNames = QFileDialog::getOpenFileNames(this, tr("Open Files"));
    addToPlaylist(fileNames);

    if(playList->mediaCount() != 0) {
        plyBtn->setEnabled(true);
        int val = 30;
        initVolume(val);
        initSlider(val);

        videoListWidget->setCurrentRow(0);

        preBtn->setEnabled(true);
        nextBtn->setEnabled(true);
    }
    qDebug() << "播放列表视频数量:" << playList->mediaCount();
}

void MyMedia::on_next_clicked() {
//    player->stop();
    if(playList->currentIndex() == playList->mediaCount() - 1) {
        playList->setCurrentIndex(0);
    } else {
        playList->next();
    }


    int nextRow = 0;
    int curRow = videoListWidget->currentRow();
    if(curRow == videoListWidget->count() - 1) {
        nextRow = 0;
    } else {
        nextRow = curRow + 1;
    }
    videoListWidget->setCurrentRow(nextRow);

    player->play();

}

void MyMedia::on_pre_clicked() {
//    player->stop();
    if(playList->currentIndex() == 0) {
        playList->setCurrentIndex(playList->mediaCount() - 1);
    } else {
        playList->previous();
    }

    int preRow = 0;
    int curRow = videoListWidget->currentRow();
    if(curRow == 0) {
        preRow = videoListWidget->count() - 1;
    } else {
        preRow = curRow - 1;
    }

    videoListWidget->setCurrentRow(preRow);


    player->play(); 
}

//TODO
void MyMedia::on_videoListWidget() {

    int curRow = videoListWidget->currentRow();
    playList->setCurrentIndex(curRow);

    showVideo(true);
}

void MyMedia::showVideo(bool state) {
    if(state) {
        player->play();
        isPlay = false;
        plyBtn->setText("pause");
    } else {
        player->pause();
        isPlay = true;
        plyBtn->setText("play");
    }



}

void MyMedia::addToPlaylist(const QStringList& fileNames) {
    videoListWidget->addItems(fileNames);

    foreach (QString const &argument, fileNames) {
        QFileInfo fileInfo(argument);
        if (fileInfo.exists()) {
            QUrl url = QUrl::fromLocalFile(fileInfo.absoluteFilePath());
            if (fileInfo.suffix().toLower() == QLatin1String("m3u")) {
                playList->load(url);
            } else
                playList->addMedia(url);
        } else {
            QUrl url(argument);
            if (url.isValid()) {
                playList->addMedia(url);
            }
        }
    }
}

void MyMedia::contextMenuEvent(QContextMenuEvent *event) {

    QMenu *popMenu = new QMenu(this);

    if(videoListWidget->itemAt(videoListWidget->mapFromParent(event->pos()))) {

        popMenu->addAction(new QAction("删除", this));
        connect(popMenu, SIGNAL(triggered(QAction *)), this, SLOT(delFile()));
    }
    popMenu->exec(QCursor::pos());

}

void MyMedia::delFile() {


    videoListWidget->takeItem(videoListWidget->currentRow());
    videoListWidget->removeItemWidget(videoListWidget->currentItem());

    if(!videoListWidget->count()) {
        preBtn->setEnabled(false);
        nextBtn->setEnabled(false);
        plyBtn->setEnabled(false);
    }

    player->stop();
}


/*
 * @Brief:
 * @Param:  初始化
 * @Return: NULL
 */
void MyMedia::initUi() {


    //初始
    mainLayout = new QVBoxLayout;
    layout = new QHBoxLayout;
    bottomLayout = new QHBoxLayout;

    plyBtn = new QPushButton;
    openBtn = new QPushButton;
    preBtn = new QPushButton;
    nextBtn = new QPushButton;

    videoWidget = new QVideoWidget;
    videoListWidget = new QListWidget;

    player = new QMediaPlayer;
    playList = new QMediaPlaylist();

    voluSlider = new QSlider();
    progressSlider = new QSlider();

    delAction = new QAction("删除", this);


    //设置文本
    openBtn->setText("open");
    plyBtn->setText("play");
    plyBtn->setEnabled(false);
    preBtn->setEnabled(false);
    nextBtn->setEnabled(false);
    preBtn->setText("pre");
    nextBtn->setText("next");

    player->setPlaylist(playList);
    player->setVideoOutput(videoWidget);

    voluSlider->setOrientation(Qt::Horizontal);
    progressSlider->setOrientation(Qt::Horizontal);

    videoListWidget->setFixedSize(150, 500);


    //按钮布局
    bottomLayout->addWidget(openBtn);
    bottomLayout->addWidget(plyBtn);
    bottomLayout->addWidget(preBtn);
    bottomLayout->addWidget(nextBtn);

    bottomLayout->addWidget(voluSlider);
    bottomLayout->addWidget(progressSlider);

    layout->addWidget(videoWidget);
    layout->addWidget(videoListWidget);


//    mainLayout->addWidget(videoWidget);
    mainLayout->addLayout(layout);
    mainLayout->addLayout(bottomLayout);

    this->setLayout(mainLayout);

    //连接
    connect(plyBtn, SIGNAL(clicked()), this, SLOT(on_ply_clicked()));
    connect(openBtn, SIGNAL(clicked()), this, SLOT(on_open_clicked()));
    connect(preBtn, SIGNAL(clicked()), this, SLOT(on_pre_clicked()));
    connect(nextBtn, SIGNAL(clicked()), this, SLOT(on_next_clicked()));
    connect(voluSlider, SIGNAL(valueChanged(int)), this, SLOT(on_voluSlider(int)));
    connect(progressSlider, SIGNAL(valueChanged(int)), this, SLOT(on_progressSlider(int)));
    connect(videoListWidget, SIGNAL(itemClicked(QListWidgetItem *)), this, SLOT(on_videoListWidget()));
}

#ifndef MYMEDIA_H
#define MYMEDIA_H

#include <QWidget>
#include <QPushButton>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QVideoWidget>
#include <QMediaPlayer>
#include <QMediaPlaylist>
#include <QSlider>
#include <QListWidget>

class MyMedia : public QWidget
{
    Q_OBJECT
public:
    explicit MyMedia(QWidget *parent = nullptr);

    void initUi();
    void addToPlaylist(const QStringList& fileNames);


public slots:
    void on_ply_clicked();
    void on_open_clicked();
    void on_next_clicked();
    void on_pre_clicked();

    void on_voluSlider(int val);
    void on_progressSlider(int val);

    void initVolume(int val);
    void initSlider(int val);

    void showVideo(bool state);
    void on_videoListWidget();

    void delFile();


private:

    int currentIndex;

    bool isPlay;

    QPushButton *plyBtn;
    QPushButton *openBtn;
    QPushButton *nextBtn;
    QPushButton *preBtn;

    QSlider *voluSlider;
    QSlider *progressSlider;

    QVideoWidget *videoWidget;

    QListWidget *videoListWidget;



    //
    QVBoxLayout *mainLayout;
    QHBoxLayout *layout;
    QHBoxLayout *bottomLayout;

    QMediaPlayer *player;
    QMediaPlaylist *playList;

    //
    QAction *delAction;

protected:
    virtual void contextMenuEvent(QContextMenuEvent *event);
};

#endif // MYMEDIA_H
